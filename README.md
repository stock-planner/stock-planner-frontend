# stock-planner-frontend

## Environment Variables
This project uses environment variables.
Local development variables are stored inside [public/env.json](public/env.json).
Variables are overwritten in production when the Docker container starts (see [entrypoint.js](entrypoint.js)).

NOTE: In production, only variables starting with "APP_" will be injected!

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
