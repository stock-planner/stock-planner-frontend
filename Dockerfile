FROM nginx:1.21-alpine

RUN apk add --update nodejs

COPY nginx.conf /etc/nginx/nginx.conf
COPY entrypoint.js /bin/entrypoint.js

WORKDIR /app
COPY dist /app

EXPOSE 80
CMD ["node", "/bin/entrypoint.js"]
