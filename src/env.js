import axios from "axios";

const env = {};
export default env;

export async function initEnv() {
  Object.assign(env, (await axios.get("/env.json")).data);
}
