import Vue from 'vue'
import VueRouter from 'vue-router'
import Sectors from "../views/Sectors";
import SectorsBoxplot from "../views/SectorsBoxplot";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/sectors'
  },
  {
    path: '/sectors/performance-plot',
    name: 'SectorsBoxplot',
    component: SectorsBoxplot
  },
  {
    path: '/sectors',
    name: 'Sectors',
    component: Sectors
  },
  {
    path: '/sectors/:id',
    name: 'Sector',
    component: Sectors
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
