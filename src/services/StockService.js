import axios from "axios";
import env from "@/env";

export default class StockService {

  async getSectors() {
    const sectors = await axios.get(`${env.APP_BACKEND_URL}/sectors`);
    return sectors.data;
  }

  async getStocksBySector(sectorId) {
    const stocks = await axios.get(`${env.APP_BACKEND_URL}/sectors/${sectorId}/stocks`);
    return stocks.data;
  }

}
