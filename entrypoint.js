const fs = require("fs");
const { spawn } = require("child_process");

const env = JSON.parse(fs.readFileSync("env.json", "utf-8"));
for (const [key, value] of Object.entries(process.env)) if(key.startsWith("APP_")) env[key] = value;
fs.writeFileSync("env.json", JSON.stringify(env, null, 2));

spawn("nginx", ["-g", "daemon off;"], { stdio: "inherit" });
